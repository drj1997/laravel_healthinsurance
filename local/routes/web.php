<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/search','StatecityController@state');

Route::get('/products','ProductsController@index');

Route::get('/json-cities','StatecityController@city');

Route::get('/hospitals-search/{state_id}/{city_id}','StatecityController@gethospitals');

Route::get('/payment',function(){
	return view('payment');
});

Route::get('/view/{productid}','ProductsController@view');