<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductfeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productfeatures', function (Blueprint $table) {
            $table->string('productid')->unique();
            $table->string('featureid')->unique();
            $table->string('featurevalue');
            $table->string('createdby');
            $table->date('createddate');
            $table->string('lastmodifiedby');
            $table->date('lastmodifieddate');
             $table->foreign('featureid')->references('featureid')->on('featuresstaticmaster');
             $table->foreign('productid')->references('productid')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productfeatures');
    }
}
