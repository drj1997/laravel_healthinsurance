<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInsurancecompaniesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('insurancecompanies', function (Blueprint $table) {
            $table->string('companyid')->unique();
            $table->string('companyname');
            $table->string('bussinesstype');
            $table->string('incorptype');
            $table->integer('incorpyear');
            $table->string('incorpstate');
            $table->date('incorpdate');
            $table->string('webiste');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('insurancecompanies');
    }
}
