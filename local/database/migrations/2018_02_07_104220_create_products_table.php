<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
             $table->string('productid')->unique();
             $table->string('companyid');
             $table->string('pfullname');
             $table->string('pshortname');
             $table->string('targetclientgroup');
             $table->integer('price');
             $table->date('withdrawn');
             $table->string('activestatus');
             $table->string('weblink');
             $table->string('policydocument');
              $table->foreign('companyid')->references('companyid')->on('insurancecompanies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
