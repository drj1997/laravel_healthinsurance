<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitymasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citymaster', function (Blueprint $table) {
            
            $table->string('cityname');
            $table->string('citycode')->unique();
            $table->string('statec');
            $table->foreign('statec')->references('statecode')->on('statemaster');  
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citymaster');
    }
}
