<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalsnetworkTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospitalsnetwork', function (Blueprint $table) {
           
            $table->string('companyid');
            $table->string('hospitalid');
            $table->string('hospitalname')->nullable();
            $table->string('address')->nullable();
            $table->string('citycode')->nullable();
            $table->string('statecode')->nullable();
            $table->string('pincode')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('fax')->nullable();
            $table->string('contactperson')->nullable();
            $table->string('email')->nullable();
            $table->string('lattitude')->nullable();
            $table->string('longitude')->nullable();
            $table->foreign('companyid')->references('companyid')->on('insurancecompanies'); 
            $table->foreign('citycode')->references('citycode')->on('citymaster');
            $table->foreign('statecode')->references('statecode')->on('statemaster');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospitalsnetwork');
    }
}
