<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Citymaster extends Model
{
    //
    protected $table='citymaster';
    protected $fillable = [

    				'cityname',
    				'citycode',
    				'statec',
    			];
}
