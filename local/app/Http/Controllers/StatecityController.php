<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Citymaster;
use App\Statemaster;
use App\Hospital;

class StatecityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function state()
    {
        $states = statemaster::all();
        return view('search',compact('states'));
    }

     public function city()
    {
        $state_id = Input::get('state_id');
      
        $cities = citymaster::where('statec', '=', $state_id)->get();
      
        return response()->json($cities);
    }

        public function gethospitals($stateId, $CityId)
        {
        //  return response()->json([$stateId,$CityId]);
       

      
          //$city_id = Input::get('city_id');
        // return $state_id;
         $hospitals = Hospital::where([['statecode','=', $stateId],['citycode','=', $CityId]])->get();
       //  $hospitals = hospitalsnetwork::where([['statecode','=', $stateId],['citycode','=',$CityId]])->get();
        // $hospitals = citymaster::all();
       //return response()->json([$stateId,$CityId]);
         return response()->json($hospitals);
        // return json_encode(['hi'=>'hello']);
         //return view('search',compact('hospitals'));

    }

    public function products()
    {
        return view('products');
    }

    public function index()
    {
        
        return view('products');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
