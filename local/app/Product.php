<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
     protected $table='products';
    protected $fillable = [
    				'productid',
    				'companyid',
    				'pfullname',
    				'pshortname',
    				'targetclientgroup',
    				'price',
    				'withdrawn',
    				'activestatus',
    				'weblink',
    				'policydocument',
    			];
}
