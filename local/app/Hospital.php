<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospital extends Model
{
 
protected $table='hospitalsnetwork';
protected $fillable=[
				'companyid',
				'hospitalid',
				'hospitalname',
				'address',
				'citycode',
				'statecode',
				'pincode',
				'phone',
				'mobile',
				'fax',
				'contactperson',
				'email',
				'lattitude',
				'longitude',
            ];
    //
}
