<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statemaster extends Model
{
    //
    protected $table='statemaster';
    protected $fillable = [

    					'statename',
    					'statecode'];
}
