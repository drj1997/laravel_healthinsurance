@extends('layouts.app')
@section('content')
<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="utf-8">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> 
    <link rel="stylesheet" href="http://demo.itsolutionstuff.com/plugin/bootstrap-3.min.css">

        <title>Health Insurance</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <style>
                  table, th, td {
                  border: 1px solid black;
                  padding: 5px;
                  border-collapse: collapse;
                  }
              </style>
    </head>
    <body>
      <div class="container"> 
         <div class="row">
             <div class="col-sm-4" style="background-color:white;">
                <br></br> 
                <form method="get" class="form-horizontal">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="put">
                     <div>
                         <label style="background-color:lightgray;">Target Group:</label><br></br>
                         <input class="messageCheckbox" type="checkbox" name="c[]" value="farmers" />farmers<br/>
                         <input class="messageCheckbox" type="checkbox" name="c[]" value="Family Floater" />Family Floater<br/>
                         <input class="messageCheckbox" type="checkbox" name="c[]" value="Individual" />Individual<br/>
                         <input class="messageCheckbox" type="checkbox" name="c[]" value="Senior Citizens" />Senior Citizens<br/>
                         <input class="messageCheckbox" type="checkbox" name="c[]" value="Women" />Women<br/>
                     </div>
                     <br></br>
                     <div>
                          <label style="background-color:lightgray;">Companies:</label><br></br>
                          <input class="messageCheckbox1" type="checkbox" name="e[]"  value="C1" />Agriculture Insurance Co. of India Ltd.<br/>
                          <input class="messageCheckbox1" type="checkbox" name="e[]"  value="C2" />Apollo Munich Health Insurance Co. Ltd.<br/>
                          <input class="messageCheckbox1" type="checkbox" name="e[]"  value="C3" />Bajaj Allianz General Insurance Co. Ltd.<br/>
                     </div>
                     <br></br>
                    
                     
                 </form>
                  <button type="submit" value="Submit" class="btn btn-primary" id="submit">
                  Submit
              </button>
                     
             </div>
             <br></br> 
             <div class="col-sm-7" style="background-color:lightgray;">
              
                 <table border='1'><head>
                 <tr>
                <!-- <th>SNO</th> -->
                 <th>ProductID</th>
                 <th>CompanyID</th>
                 <th>Product_Full_Name</th>
                 <th>Target_ClientGroup</th>
                 <th>Price</th>
                 <th>Active_status</th>
                 <th>View Details<th>
                 </tr></head>
                 <body>
                @foreach($pro as $key => $value)
                 <tr>
                 	<tbody id="details">
               <!--  <td>{{ $loop->index+1}}</td> -->
                 <td>{{ $value->productid }}</td>
                 <td>{{ $value->companyid }}</td>
                 <td>{{ $value->pfullname }}</td>
                 <td>{{ $value->targetclientgroup }}</td>
                 <td>{{ $value->price }}</td>
                 <td>{{ $value->activestatus }}</td>
                 <td><a href="{{ url('/view',$value->productid) }}">View</a><td>
                 </tr>
                 @endforeach
                
                 </body>
             </tbody>
                 </table>
                  
            </div>
        </div>
    </div>
           

    </body>
    <script>
      $(document).ready(function()
      {
      	              
             $("#submit").click(function()
              {
              	 var tg =[]; 
                        var inputElements = document.getElementsByClassName('messageCheckbox');
                        for(var i=0; inputElements[i]; ++i){
                              if(inputElements[i].checked)
                              {
                                   tg.push(inputElements[i].value.toLowerCase());
                                   console.log(tg);
                                   
                              }
                        }
                        var cg = []; 
                        var inputElements = document.getElementsByClassName('messageCheckbox1');
                        for(var i=0; inputElements[i]; ++i){
                              if(inputElements[i].checked){
                                   cg.push(inputElements[i].value.toLowerCase());
                                   console.log(cg);
                                   
                              }
                        }
              	alert("filtering");
			   //var tg_length=tg.length;
			    //var cg_length=cg.length;
			    //console.log(tg_length);
			    //console.log(cg_length);
			    $("#details ").filter(function() 
			    {
			    var $t = $(this);
			    $(this).hide();

    			for (var d = 0; d < tg.length; ++d)
    			 {
        			if ($t.text().toLowerCase().indexOf(tg[d]) > -1) 
        			{
        				console.log('1');
        				console.log($t.text());
            			return true;
        			}

    			}

    			for (var d = 0; d < cg.length; ++d)
    			 {
        			if ($t.text().toLowerCase().indexOf(cg[d]) > -1) 
        			{
        				console.log('1');
        				console.log($t.text());
            			return true;
        			}

    			}

   				return false;
				}).show();

                               
			
          });
});





   </script>
 </html>
 @endsection('content')