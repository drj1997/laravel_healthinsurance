@extends('layouts.app')
@section('content')
<!--<h1>This is the page in which user selects state ans city and based on that list of hospitals will be displayed</h1>-->
<!DOCTYPE html>
<htmml lang="en">
	<head>
		 <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
 .table{
        margin: auto;
         border: 2px solid black;
          padding: 4px;
          border-collapse: collapse;
          margin-left: :70px;
       }
</style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Search for hospitals</div>

                <div class="panel-body">
                      <!-- start of the form-->

                    <form class="form-horizontal" method="get">
                                   {{ csrf_field()}}


                             <input type="hidden" name="_method" value="put">
                      
                            <label for="email" class="col-md-4 control-label">Select State</label>

                            <div class="col-md-6">
                               <select id="state" name="state" class="form-control">
                               	<option value="0" disable="true" selected="true"><p style="text-align :center;">=== Select State ===</p></option>
                               @foreach ($states as $key => $value)
                               	<option value="{{ $value->statecode }}">{{ $value->statename }}</option>
                               	@endforeach
                               </select>
                            </div>
                      
                            <br></br>
                        	<br></br>
                            <label for="password" class="col-md-4 control-label">Select City</label>

                            <div class="col-md-6">
                               <select id="city" name="city" class="form-control	">
                               <option value="0" disable="true" selected="true"><p style="text-align :center;">=== Select City ===</p></option>
                               </select>
                            </div>
							<br></br>
							<br>
                            <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                               
               
                            </div>
                        </div>


                    </form>
                     <button type="submit" class="btn btn-primary" id="search1">
                                 
                                   Search
                                 
                                </button>

                </div>
            </div>
        </div>
    </div>
</div>

<br></br>
<br></br>
<!-- div to display the data from the database related to list of hospitals in tabular form-->
<div class="col-sm-10">
        <table id="gre" class="table">
              <thead>
                       <tr>
                <th>CompanyID</th>
                <th>HospitalID</th>
                <th>HospitalName</th>
                <th>Address</th>
                <th>Citycode</th>
                <th>Statecode</th>
                <th>Pincode</th>
                <th>Phone</th>
                <th>Mobile</th>
                <th>Fax</th>
                <th>ContactPerson</th>
                <th>Email</th>
                <th>Lattitude</th>
                <th>Longitude</th>
            </tr>
                                </thead>
                                <tbody id="tb">
                                </tbody>
        </table>
     </div>

     <br></br>

 <script type="text/javascript">
 $(document).ready(function()
 	{
      $('#state').on('change', function(e)
           {
        console.log("hello");
        console.log(e);
        var state_id = e.target.value;
        console.log(state_id);
        $.get('/json-cities?state_id=' + state_id,function(data) 
       			 {
				          console.log(data);
				          $('#city').empty();
				          $('#city').append('<option value="0" disable="true" selected="true">=== Select City ===</option>');

			          $.each(data, function(index, regenciesObj)
			          {
			            $('#city').append('<option value="'+ regenciesObj.citycode +'">'+ regenciesObj.cityname +'</option>');
			          })
               });
            });
      });
  </script>





<script>
$(document).ready(function()
  {
         $("#search1").click(function()
               {
                alert("search is processing");//to check whether the search fucntion is triggered or not
                    console.log("hello");
                    var state_id=$('#state').val();
                    var city_id=$('#city').val();
                    console.log(state_id);
                    console.log(city_id);

                                  $.ajax({
                                            url: '/hospitals-search/'+state_id+'/'+city_id,
                                            type: "get",
                                            dataType:'JSON',
                                            success: function(response)
                                             {
                                              console.log(response[0]);
                                              var tbd=document.getElementById("tb");
                                              tbd.innerHTML="";
                                              var temp="";
                                              for(var i=0;i<response.length;i++){

                                                                var k= JSON.parse(JSON.stringify(response[i]));
                                                                temp+="<tr>";
                                                                $.each(k,function(key,value){
                                                                                temp+="<td>"+value+"</td>";
                                        
                                                                });
                                       
                                               temp+="</tr>";
                                                }

                                                tbd.innerHTML=temp;
                                           //f(data == "success")
                                            //alert(response); 
                                             },
                                            error: function(response){
                                            alert('Error'+response);
                                            }
                                          });
                                   });
});

</script>

</body>
</html>

@endsection('content')